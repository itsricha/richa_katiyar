package com.example.roomdemo.db

class SubscriberRepository(private val dao:SubscriberDao) {
    val subscribersDAO = dao.getAllSubscriber()

    suspend fun insertdata(subscriber: Subscriber){
        dao.insertSubscriber(subscriber)
    }

    suspend fun updatedata(subscriber: Subscriber){
        dao.updateSubscriber(subscriber)

    }
    suspend fun deletedata(subscriber: Subscriber){
        dao.deleteSubscriber(subscriber)

    }
    suspend fun deletealldata(){
        dao.deleteAll()
    }
}