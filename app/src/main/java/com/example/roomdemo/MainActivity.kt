package com.example.roomdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.roomdemo.databinding.ActivityMainBinding
import com.example.roomdemo.db.Subscriber
import com.example.roomdemo.db.SubscriberDatabase
import com.example.roomdemo.db.SubscriberRepository

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: SubscriberViewModel
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_main)

        val dao= SubscriberDatabase.getInstance(application).subscriberDao
        val repository=SubscriberRepository(dao)
        val factory= MyViewModelFactory(repository)

        viewModel =ViewModelProvider(this,factory).get(SubscriberViewModel::class.java)

        binding.myViewModel =viewModel
        binding.lifecycleOwner=this

        initRecyclerview()

    }

    private fun initRecyclerview(){
        binding.subscriberRecyclerview.layoutManager = LinearLayoutManager(this)
        displaydata()
    }

    private fun displaydata(){
        viewModel.subscriber.observe(this, Observer {
            Log.i("MyTag",it.toString())
            binding.subscriberRecyclerview.adapter =MyRecyclerViewAdapter(it,{selectedItem:Subscriber->itemClicked(selectedItem)})
        })
    }

    private fun itemClicked(subscriber: Subscriber){
       Toast.makeText(this,"Clicked Item ${subscriber.name}",Toast.LENGTH_LONG).show()
        viewModel.initUpdateAndDelete(subscriber)
    }
}