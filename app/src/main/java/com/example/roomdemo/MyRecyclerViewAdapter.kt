package com.example.roomdemo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.roomdemo.databinding.ListItemsRecyclerviewBinding
import com.example.roomdemo.db.Subscriber

class MyRecyclerViewAdapter(private val subscriberslist: List<Subscriber>, private val clicklistener:(Subscriber)-> Unit) : RecyclerView.Adapter<MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
     val layoutInflater =LayoutInflater.from(parent.context)
     val binding: ListItemsRecyclerviewBinding= DataBindingUtil.inflate(layoutInflater,R.layout.list_items_recyclerview,parent,false)
     return MyViewHolder(binding)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
    holder.bind(subscriberslist[position],clicklistener)
    }

    override fun getItemCount(): Int {
        return subscriberslist.size
    }

}

class MyViewHolder(val binding: ListItemsRecyclerviewBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(subscriber: Subscriber,clicklistener:(Subscriber)-> Unit) {
        binding.nameText.text = subscriber.name
        binding.emailText.text = subscriber.email
        binding.listItemLayout.setOnClickListener(){
          clicklistener(subscriber)
        }
    }
}