package com.example.roomdemo

import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.roomdemo.db.Subscriber
import com.example.roomdemo.db.SubscriberRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class SubscriberViewModel(private val repository: SubscriberRepository): ViewModel(),Observable{

    val subscriber= repository.subscribersDAO

    private var isUpdateorDelete =false
    private lateinit var subscriberToUpdateandDelete :Subscriber

    @Bindable
    val inputName = MutableLiveData<String>()

    @Bindable
    val inputEmail = MutableLiveData<String>()

    @Bindable
    val saveButton = MutableLiveData<String>()

    @Bindable
    val deleteButton = MutableLiveData<String>()

    init {
        saveButton.value= "Save"
        deleteButton.value="Clear All"
    }

    fun saveorUpdateData(){
        if (isUpdateorDelete){
            subscriberToUpdateandDelete.name= inputName.value!!
            subscriberToUpdateandDelete.email= inputEmail.value!!
            update(subscriberToUpdateandDelete)
        }else {
            val name = inputName.value!!
            val email = inputEmail.value!!
            insert(Subscriber(0, name, email))
            inputName.value = null
            inputEmail.value = null
        }

    }

    fun clearAllorDeletedata(){
        if (isUpdateorDelete){
            delete(subscriberToUpdateandDelete)
        }else{
        deleteAll()
        }
    }

    fun insert(subscriber :Subscriber):Job= viewModelScope.launch {
            repository.insertdata(subscriber)
        }

    fun delete(subscriber :Subscriber):Job= viewModelScope.launch {
        repository.deletedata(subscriber)
        inputName.value=null
        inputEmail.value=null
        isUpdateorDelete=false
        saveButton.value="Save"
        deleteButton.value="Clear All"
    }

    fun update(subscriber :Subscriber):Job= viewModelScope.launch {
        repository.updatedata(subscriber)
        inputName.value=null
        inputEmail.value=null
        isUpdateorDelete=false
        saveButton.value="Save"
        deleteButton.value="Clear All"
    }

    fun deleteAll():Job= viewModelScope.launch {
        repository.deletealldata()
    }

    fun initUpdateAndDelete(subscriber: Subscriber){
        inputName.value=subscriber.name
        inputEmail.value=subscriber.email
        isUpdateorDelete=true
        subscriberToUpdateandDelete=subscriber
        saveButton.value="Update"
        deleteButton.value="Delete"

    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

}